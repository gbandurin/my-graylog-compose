# My Graylog dkr-compose

This repository contains ready-to-go configuration to quickly start Graylog in container with docker-compose. You can change configuration files of graylog suitable for your needs.  

## Prerequisites

You need to have docker and docker-compose installed on your machine.

## Variables

```
PEPPER=<your_password_pepper_16_chars_minimum> 
ROOTPASS=<SHA2_hashsum_of_your_password>
```

Add them to the .env file next to the docker-compose.yml.

## Usage

```
docker-compose up -d
```

## License

MIT

## Disclaimer

This configuration was made for testing purposes and may not be suitable for production. You take responsibility for any problems, caused by this configuration or misconfiguration of this configuration. Use it at your own risc. 

## Maintainer

George Bandurin (gbandurin@yandex.ru)
